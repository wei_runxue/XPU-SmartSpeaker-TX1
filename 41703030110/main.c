#include <stdio.h>
#include <curl/curl.h> //bcurl的头文件

int main(void)
{
    //以只写方式打开文件
    FILE* fp = fopen("hello.txt", "w");
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("fopen() failed");
        return 1;
    }

    //将字符串写入文件
    fprintf(fp, "Hello world\n");

    //关闭文件
    fclose(fp);

    //初始化libcurl
    CURL* curl = curl_easy_init();
    if (curl == NULL)
    {
        perror("curl_easy_init() failed");
        return 1;
    }

    //准备HTTP请求消息，设置API地址（URI）
    curl_easy_setopt(curl, CURLOPT_URL, "http://www.nmc.cn/f/rest/aqi/57036");

    //发送HTTP请求消息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        return 1;
    }

    //释放libcurl申请的资源
    curl_easy_cleanup(curl);

    return 0;
}