#include<stdio.h>
#include<curl/curl.h>//licurl头文件
#include "cJSON.h"//cJSON的头文件
#include <stdlib.h>//free使用的头文件
#include<string.h>


//使用api_key和secret_key获取ACCESS_TOKEN
char* get_token(const char* api_key,const char* secret_key)
{
     FILE* fp;

    //响应消息的地址
    char* response = NULL;
    //响应消息的长度
    size_t resplen = 0;
    //创建内存文件，当通过文件句柄写入数据时，会自动分配内存
    fp = open_memstream(&response, &resplen);
    if (fp==NULL)//打开文件夹失败，打印错误信息并退出
    {
            perror("open_memstream() failed");
            return NULL;
    }

    //初始化HTTP客户端
    CURL* curl = curl_easy_init();
    

char* uri=NULL;
//拼接URI
asprintf(&uri,
         "https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s",
         api_key,
         secret_key);

//准备HTTP请求消息,设置API的地址(URL）
curl_easy_setopt(curl,CURLOPT_URL,uri);


curl_easy_setopt(curl,CURLOPT_WRITEDATA,fp);
//发送HTTP请求消息，等待服务器的响应消息
CURLcode error=curl_easy_perform(curl);
if(error !=CURLE_OK)
{
    fprintf(stderr,"curl_easy_perform() failed:%s\n",curl_easy_strerror(error));
    curl_easy_cleanup(curl);
    free(uri);
    fclose(fp);
    free(response);
    return NULL;
}
//释放libcurl申请的资源
    curl_easy_cleanup(curl);
    //释放asprintf分配的内存
    free(uri);

    //关闭文件
    fclose(fp);

    //puts(response);
    /*{
    "refresh_token": "25.b2e5253042569d6dbb188ab0ea81beef.315360000.1909120683.282335-20728144",
    "expires_in": 2592000,
    "session_key": "9mzdX7vBBnDL+pEVq4KkByJMOwQbxdkqrKFvAYIHclCK4uUzAG94tZZYAqgfmjaagGgly+3QalDyLfGDZCW6rQxlSjCPgQ==",
    "access_token": "24.dac312c21048383077e12b5b902c893f.2592000.1596352683.282335-20728144",
    "scope": "audio_voice_assistant_get brain_enhanced_asr audio_tts_post brain_speech_realtime public brain_all_scope picchain_test_picchain_api_scope brain_asr_async wise_adapt lebo_resource_base lightservice_public hetu_basic lightcms_map_poi kaidian_kaidian ApsMisTest_Test\u6743\u9650 vis-classify_flower lpq_\u5f00\u653e cop_helloScope ApsMis_fangdi_permission smartapp_snsapi_base iop_autocar oauth_tp_app smartapp_smart_game_openapi oauth_sessionkey smartapp_swanid_verify smartapp_opensource_openapi smartapp_opensource_recapi fake_face_detect_\u5f00\u653eScope vis-ocr_\u865a\u62df\u4eba\u7269\u52a9\u7406 idl-video_\u865a\u62df\u4eba\u7269\u52a9\u7406",
    "session_secret": "3c8356b42c24cb38202675394805cf49"
}*/

    cJSON* json=cJSON_Parse(response);
    if(json==NULL)
   {
     const char* error_pos=cJSON_GetErrorPtr();
     if(error_pos!=NULL)
     {
       fprintf(stderr," Error before:%\n",error_pos);
     }
     free(response);
    return NULL;
    }

    //获取access_token字段
    cJSON* access_token=cJSON_GetObjectItemCaseSensitive(json,"access_token");
    if(!cJSON_IsString(access_token))//如果不存在access_token,进行出错处理
    {
        cJSON* error_description=cJSON_GetObjectItemCaseSensitive(json,"error_description");
        fprintf(stderr,"%s\n",error_description->valuestring);
        return NULL;
    }
    //复制token字符串,使用完之后需要使用free函数释放内存
    char* token= strdup(access_token->valuestring);

    free(response);
    //释放cJSON数据结构占用的内存
    cJSON_Delete(json);

    return token;
}
//将文本转换为语音
void text2speech(const char* token,const char* text)
{
    CURL* curl=curl_easy_init();
    //发送到百度云的字符串需要进行两次URI编码
    char* temp=curl_easy_escape(curl,text,strlen(text));
    char* data=curl_easy_escape(curl,temp,strlen(temp));
    curl_free(temp);
    //拼接POST请求发送的数据
    char* postdata;
    asprintf(&postdata,"tex=%s&lan=zh&cuid=***&ctp=1&aue=6&tok=%s",data,token);
    //启动播放软件，通过管道写入音频数据
    FILE* fp=popen("aplay -q -","w");
    if(fp==NULL)
    {
        perror("fopen failed");
        return;
    }
    curl_easy_setopt(curl,CURLOPT_URL,"https://tsn.baidu.com/text2audio");
    //配置客户端，使用HTTP的POST方法发送请求消息
    curl_easy_setopt(curl,CURLOPT_POST,1);
    //配置需要通过POST请求消息发送给服务器的数据
    curl_easy_setopt(curl,CURLOPT_POSTFIELDS,postdata);
    curl_easy_setopt(curl,CURLOPT_WRITEDATA,fp);
    //发送HTTP请求消息，等待服务器的响应消息
    CURLcode error=curl_easy_perform(curl);
    if(error !=CURLE_OK)
    {
      fprintf(stderr,"curl_easy_perform() failed:%s\n",curl_easy_strerror(error));
      curl_easy_cleanup(curl);
      free(postdata);
      pclose(fp);
      return;
    }
    //释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);
    free(postdata);
    //关闭管道
    pclose(fp);
    //调用播放器播放音频文件
    //system("aplay -q test.wav");

}
//百度语音合成API，一次最多可以转换2048个字符
#define LINE_LEN 2048

//保存输入字符串的缓冲区
char line[LINE_LEN];

int main()
{   
    char* token=get_token("tRkokZefGzj4a6RQz1cj0LPC","5Q7sh7GK3I6pVH09xR1bxSeKgWhPXRcs");
    if(NULL==token)
     
     {
         return EXIT_FAILURE;
     }
    //puts(token);
    //从标准输入读取一行字符
    while(fgets(line,LINE_LEN,stdin)!=NULL)
    {
       //将读入的文本转化为语音
       text2speech(token,line);
    }

    free(token);

    return 0;
}