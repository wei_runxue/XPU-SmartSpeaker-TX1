#include <stdio.h>
#include <stdlib.h> //free使用的头文件
#include <string.h> //strdup
#include <curl/curl.h> //libcurl的头文件
#include "cJSON.h"
char* get_token(const char* api_key, const char* secret_key)
{
    FILE* fp;

    //以只写方式打开文件
    //fp = fopen("hello.txt", "w");

    //响应消息的地址
    char* response = NULL;
    //响应消息的长度
    size_t resplen = 0;
    //创建内存文件，当通过文件句柄写入数据时，会自动分配内存
    fp = open_memstream(&response, &resplen);
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("open_memstream() failed");
        return NULL;
    }
    //初始化HTTP客户端
    CURL* curl = curl_easy_init();
    
     char* uri = NULL;
    //拼接URI，asprintf会自动分配内存，并将要打印的字符串保存到内存中。
    asprintf(&uri, 
             "https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s",
             api_key,
             secret_key);
    //准备HTTP请求消息，设置API地址（URI）
    curl_easy_setopt(curl, CURLOPT_URL,uri);
    //如果不指定写入的文件，libcurl会把服务器响应消息中的内容打印到屏幕上
    //如果指定了文件句柄，libcurl会把服务器响应消息中的内容写入文件
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    //发送HTTP请求消息，等待服务器的响应消息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        //释放asprintf分配的内存
        free(uri);
        fclose(fp);
        free(response);
        return NULL;
    }

    //释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);
    //释放asprintf分配的内存
    free(uri);
    //关闭内存文件
    fclose(fp);

    //puts(response);
    cJSON* json = cJSON_Parse(response);
    if (json == NULL)
    {
        const char* error_pos = cJSON_GetErrorPtr();
        if (error_pos != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_pos);
        }
        free(response);
        return NULL;
    }
     cJSON* access_token = cJSON_GetObjectItemCaseSensitive(json, "access_token");
     if (!cJSON_IsString(access_token)) //如果不存在access_token字段，进行错误处理
    {
        cJSON* error_description = cJSON_GetObjectItemCaseSensitive(json, "error_description");
        fprintf(stderr, "%s\n", error_description->valuestring);
        return NULL;
    }
    //复制token字符串，使用完之后需要使用free函数释放此内存
    char* token = strdup(access_token->valuestring);

    free(response);
    //释放cjson数据结构占用的内存
    cJSON_Delete(json);

    return token;

}

  //将文本转换为语音
void text2speech(const char* token, const char* text)
{
   CURL* curl = curl_easy_init();

   //发送到百度云的字符串需要进行2次URL编码
    char* temp = curl_easy_escape(curl, text, strlen(text));
    char* data = curl_easy_escape(curl, temp, strlen(temp));
    curl_free(temp);
  
  //拼接POST请求发送的数据
    char* postdata;
    asprintf(&postdata, "tex=%s&lan=zh&cuid=hqyj&ctp=1&aue=6&tok=%s", data, token);
   curl_free(data);
   
    //启动播放软件，通过管道写入音频数据
    FILE* fp = popen("aplay -q -", "w");
    if (fp == NULL)
    {
        perror("fopen() failed");
        return;
    }


    curl_easy_setopt(curl, CURLOPT_URL, "https://tsn.baidu.com/text2audio");
    //配置客户端，使用HTTP的POST方法发送请求消息
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    //配置需要通过POST请求消息发送给服务器的数据
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postdata);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    //发送HTTP请求消息，等待服务器的响应消息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        free(postdata);
        pclose(fp);
        return;
    }

    //释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);
    free(postdata);

    //关闭管道
    pclose(fp);
   //调用播放器播放音频文件
    //system("aplay -q test.wav");

}

#define LINE_LEN 2048

char line[LINE_LEN];

int main()
{
    char* token = get_token("orBxOGX1ZMoFDrZy8pAGCrQq", "EidMapsyRBDN5LNplBqDSns73htomWj3");
if (NULL == token)
    {
        return EXIT_FAILURE;
    }
    //puts(token);
    //从标准输入读取一行字符
    while(fgets(line, LINE_LEN, stdin) !=NULL)
    {
    //将读入的文本转换为语音
    text2speech(token, line);
    }

    free(token);
   
    return 0;
}
