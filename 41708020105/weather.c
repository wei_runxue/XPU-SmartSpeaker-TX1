#include <stdio.h>
#include <stdlib.h> //malloc使用的头文件
#include <curl/curl.h> //libcurl的头文件
#include "cJSON.h" //cJSON的头文件

int main(void)
{
    FILE* fp;

    //以只写方式打开文件
    //fp = fopen("hello.txt", "w");

    //响应消息的地址
    char* response = NULL;
    //响应消息的长度
    size_t resplen = 0;
    //创建内存文件，当通过文件句柄写入数据时，会自动分配内存
    fp = open_memstream(&response, &resplen);
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("open_memstream() failed");
        return EXIT_FAILURE;
    }

    //初始化HTTP客户端
    CURL* curl = curl_easy_init();
    if (curl == NULL)
    {
        perror("curl_easy_init() failed");
        return EXIT_FAILURE;
    }

    //准备HTTP请求消息，设置API地址（URI）
    curl_easy_setopt(curl, CURLOPT_URL, "http://www.nmc.cn/f/rest/real/52889");
    //如果不指定写入的文件，libcurl会把服务器响应消息中的内容打印到屏幕上
    //如果指定了文件句柄，libcurl会把服务器响应消息中的内容写入文件
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    //打印HTTP请求和响应消息头
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);

    //发送HTTP请求消息，等待服务器的响应消息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        return EXIT_FAILURE;
    }

    //释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);

    //关闭内存文件
    fclose(fp);

    puts(response);

    //解析JSON字符串
    cJSON* json = cJSON_Parse(response);
    if (json == NULL)
    {
        const char* error_pos = cJSON_GetErrorPtr();
        if (error_pos != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_pos);
        }
        return EXIT_FAILURE;
    }

    /*
    {
        "station":
            { 
                "code":"52889","province":"甘肃省","city":"兰州","url":"/publish/forecast/AGS/lanzhou.html"
            },

        "publish_time":"2020-07-02 16:15",
         "weather":
            {
                 "temperature":33.1,"temperatureDiff":7.0,"airpressure":839.0,"humidity":18.0,"rain":0.0,"rcomfort":71,"icomfort":1,"info":"晴","img":"0","feelst":29.2
            },
        "wind":
            {
                "direct":"东南风","power":"微风","speed":""
            },
        "warn":
            {
                "alert":"9999","pic":"9999","province":"9999","city":"9999","url":"9999","issuecontent":"9999","fmeans":"9999","signaltype":"9999","signallevel":"9999","pic2":"9999"
            }
    }
    
    */

    cJSON* station  = cJSON_GetObjectItemCaseSensitive(json, "station");

    cJSON* province  = cJSON_GetObjectItemCaseSensitive(station, "province");

    cJSON* city  = cJSON_GetObjectItemCaseSensitive(station, "city");

    cJSON* publish_time  = cJSON_GetObjectItemCaseSensitive(json, "publish_time");

    cJSON* weather = cJSON_GetObjectItemCaseSensitive(json, "weather");

    cJSON* temperature = cJSON_GetObjectItemCaseSensitive(weather, "temperature");

    cJSON* temperatureDiff = cJSON_GetObjectItemCaseSensitive(weather, "temperatureDiff"); 

    cJSON* airpressure = cJSON_GetObjectItemCaseSensitive(weather, "airpressure");

    cJSON* humidity = cJSON_GetObjectItemCaseSensitive(weather, "humidity");

    cJSON* info = cJSON_GetObjectItemCaseSensitive(weather, "info");

    cJSON* wind = cJSON_GetObjectItemCaseSensitive(json, "wind");

    cJSON* direct = cJSON_GetObjectItemCaseSensitive(wind, "direct");

    cJSON* power = cJSON_GetObjectItemCaseSensitive(wind, "power");
   
    printf("省份: %s\n", province->valuestring);
    printf("城市: %s\n", city->valuestring);
    printf("时间: %s\n", publish_time->valuestring);
    printf("温度: %d\n", temperature->valueint);
    printf("最大温差: %d\n", temperatureDiff->valueint);
    printf("大气压: %d\n", airpressure->valueint);
    printf("湿度: %d\n", humidity->valueint);
    printf("天气: %s\n", info->valuestring);
    printf("风向: %s\n", direct->valuestring);
    printf("风级: %s\n", power->valuestring);



    //释放json数据结构占用的内存
    cJSON_free(json);

    free(response);

    return EXIT_SUCCESS;
}
