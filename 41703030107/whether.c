//7月2日作业
#include <stdio.h>
#include <stdlib.h> //malloc使用的头文件
#include <curl/curl.h> //libcurl的头文件
#include "cJSON.h" //cJSON的头文件

int main(void)
{
    FILE* fp;
#if 1
    //以只写方式打开文件
    fp = fopen("wether.txt", "w");
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("fopen() failed");
        return EXIT_FAILURE;
    }

    //初始化HTTP客户端
    CURL* curl = curl_easy_init();
    if (curl == NULL)
    {
        perror("curl_easy_init() failed");
        return EXIT_FAILURE;
    }

    //准备HTTP请求消息，设置API地址（URI）
    curl_easy_setopt(curl, CURLOPT_URL, "http://www.nmc.cn/f/rest/aqi/57245");
    //如果不指定写入的文件，libcurl会把服务器响应消息中的内容打印到屏幕上
    //如果指定了文件句柄，libcurl会把服务器响应消息中的内容写入文件
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

    //发送HTTP请求消息，等待服务器的响应消息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        return EXIT_FAILURE;
    }

    //释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);

    //关闭文件
    fclose(fp);
#endif
    //以只读方式打开文件
    fp = fopen("wether.txt", "r");
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("fopen() failed");
        return EXIT_FAILURE;
    }

    //获取文件大小
    //将文件指针定位到文件末尾
    fseek(fp, 0, SEEK_END);
    //获取文件指针的当前位置，即文件的大小
    long size = ftell(fp);

    //分配文件大小相同的内存空间
    char* jsonstr = malloc(size);

    //将文件指针重新定位到文件头
    fseek(fp, 0, SEEK_SET);
    //将文件中的内容读取到内存中
    if (fread(jsonstr, 1, size, fp) == 0)
    {
        perror("fread() failed");
        return EXIT_FAILURE;
    }

    fclose(fp);

    puts(jsonstr);

    //解析JSON字符串
    cJSON* json = cJSON_Parse(jsonstr);
    if (json == NULL)
    {
        const char* error_pos = cJSON_GetErrorPtr();
        if (error_pos != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_pos);
        }
        return EXIT_FAILURE;
    }

    cJSON* forecasttime = cJSON_GetObjectItemCaseSensitive(json, "forecasttime");

    cJSON* aqi = cJSON_GetObjectItemCaseSensitive(json, "aqi");

	cJSON* aq = cJSON_GetObjectItemCaseSensitive(json, "aq");

    cJSON* text = cJSON_GetObjectItemCaseSensitive(json, "text");

	cJSON* aqiCode = cJSON_GetObjectItemCaseSensitive(json, "aqiCode");

    printf("forecasttime: %s\n", forecasttime->valuestring);
    printf("aqi: %d\n", aqi->valueint);
	printf("aq: %d\n", aq->valueint);
	printf("text: %s\n", text->valuestring);
	printf("aqiCode: %s\n", aqiCode->valuestring);


    //释放json数据结构占用的内存
    cJSON_free(json);

    return EXIT_SUCCESS;
}